package com.example.clearscoretest.di

import com.example.clearscoretest.data.remote.ScoreApi
import com.example.clearscoretest.data.repository.ScoreRepositoryImp
import com.example.clearscoretest.domain.repository.ScoreRepository
import com.example.clearscoretest.domain.use_case.GetScoreUseCase
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ScoreModule {

    @Provides
    @Singleton
    fun provideGetScoreUseCase(repository: ScoreRepository): GetScoreUseCase {
        return GetScoreUseCase(repository)
    }

    @Singleton
    @Provides
    fun provideGsonBuilder(): Gson {
        return  GsonBuilder()
            .create()
    }

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson): Retrofit.Builder{
        return Retrofit.Builder()
            .baseUrl(ScoreApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
    }

    @Singleton
    @Provides
    fun provideScoreApiService (retrofit: Retrofit.Builder):ScoreApi {
        return retrofit
            .build()
            .create(ScoreApi::class.java)
    }

    @Provides
    @Singleton
    fun providesScoreRepository(
        api: ScoreApi
    ): ScoreRepository{
        return ScoreRepositoryImp(api)
    }
}