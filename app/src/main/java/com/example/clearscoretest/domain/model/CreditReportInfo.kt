package com.example.clearscoretest.domain.model


import com.google.gson.annotations.SerializedName

data class CreditReportInfo(
    val score: Int,
    val maxScoreValue: Int
)