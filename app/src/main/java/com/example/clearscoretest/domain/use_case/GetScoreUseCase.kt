package com.example.clearscoretest.domain.use_case

import com.example.clearscoretest.domain.model.Score
import com.example.clearscoretest.domain.repository.ScoreRepository
import com.example.clearscoretest.util.Resource
import kotlinx.coroutines.flow.Flow

class GetScoreUseCase(
    private val repository: ScoreRepository
) {
    operator fun invoke(): Flow<Resource<Score>> {
        return repository.getScore()
    }
}