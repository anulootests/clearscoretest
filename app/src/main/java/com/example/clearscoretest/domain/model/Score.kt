package com.example.clearscoretest.domain.model


import com.google.gson.annotations.SerializedName

data class Score(
    @SerializedName("creditReportInfo")
    val creditReportInfo: CreditReportInfo
)