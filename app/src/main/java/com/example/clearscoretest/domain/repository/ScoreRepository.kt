package com.example.clearscoretest.domain.repository

import com.example.clearscoretest.domain.model.Score
import com.example.clearscoretest.util.Resource
import kotlinx.coroutines.flow.Flow

interface ScoreRepository {
    fun getScore() : Flow<Resource<Score>>
}