package com.example.clearscoretest.presentation

import androidx.lifecycle.ViewModel
import com.example.clearscoretest.domain.use_case.GetScoreUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ScoreViewModel @Inject constructor(
    getScoreUseCase: GetScoreUseCase
) : ViewModel() {
    val getScoreUseCase = getScoreUseCase()
}