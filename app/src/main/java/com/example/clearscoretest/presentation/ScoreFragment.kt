package com.example.clearscoretest.presentation

import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import com.example.clearscoretest.R
import com.example.clearscoretest.databinding.ScoreFragmentBinding
import com.example.clearscoretest.domain.model.Score
import com.example.clearscoretest.util.Resource
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.InternalCoroutinesApi
import launchAndCollectIn

@AndroidEntryPoint
class ScoreFragment : Fragment() {

    private val viewModel: ScoreViewModel by viewModels()
    private var _binding : ScoreFragmentBinding?=null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScoreFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    @OptIn(InternalCoroutinesApi::class)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getScoreUseCase.launchAndCollectIn(viewLifecycleOwner, Lifecycle.State.STARTED) { result ->
            binding.loadingData.isVisible = result is Resource.Loading && result.data==null
            if(result is Resource.Success && result.data!=null){
                setValueToUI(result.data)
            }
        }
    }

    private fun setValueToUI(data: Score) {
        val maxScore = data.creditReportInfo.maxScoreValue
        val score = data.creditReportInfo.score
        setMaxScoreText(maxScore)
        setProgressBar(maxScore, score)
        setProgressText(score)
    }

    private fun setMaxScoreText(maxScore: Int) {
        binding.maxScoreText.text = getString(R.string.credit_score_max, maxScore.toString())
    }

    private fun setProgressText(score: Int) {
        binding.textViewProgress.text = score.toString()
    }

    private fun setProgressBar(
        maxScore: Int,
        score: Int
    ) {
        binding.progressBar.max = maxScore
        animateProgressBar(score)
    }

    private fun animateProgressBar(score: Int){
        val valueAnimator = ValueAnimator.ofInt(0, score)
        valueAnimator.duration = 1000
        valueAnimator.addUpdateListener {
            val animatedValue = it.animatedValue as Int
            binding.progressBar.progress = animatedValue
        }
        valueAnimator.start()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}