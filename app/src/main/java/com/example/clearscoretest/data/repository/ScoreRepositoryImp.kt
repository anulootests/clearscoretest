package com.example.clearscoretest.data.repository

import com.example.clearscoretest.data.remote.ScoreApi
import com.example.clearscoretest.domain.model.Score
import com.example.clearscoretest.domain.repository.ScoreRepository
import com.example.clearscoretest.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException

class ScoreRepositoryImp(
    private val api: ScoreApi
): ScoreRepository{
    override fun getScore(): Flow<Resource<Score>> = flow {
        var score:Score?=null
        emit(Resource.Loading(score))
        try {
            score = api.getScore().toScore()
            emit(Resource.Loading(score))
        } catch(e: HttpException) {
            emit(Resource.Error(
                message = "Oops, something went wrong!",
                data = score
            ))
        } catch(e: IOException) {
            emit(Resource.Error(
                message = "Couldn't reach server, check your internet connection.",
                data = score
            ))
        }
        emit(Resource.Success(score))
    }
}