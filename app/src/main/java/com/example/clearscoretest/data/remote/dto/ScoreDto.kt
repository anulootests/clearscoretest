package com.example.clearscoretest.data.remote.dto


import com.example.clearscoretest.domain.model.Score
import com.google.gson.annotations.SerializedName

data class ScoreDto(
    @SerializedName("creditReportInfo")
    val creditReportInfo: CreditReportInfoDto
){
    fun toScore(): Score {
        return Score(
            creditReportInfo = creditReportInfo.toCreditReportInfo()
        )
    }
}