package com.example.clearscoretest.data.remote

import com.example.clearscoretest.data.remote.dto.ScoreDto
import retrofit2.http.GET

interface ScoreApi {

    @GET("endpoint.json")
    suspend fun getScore(): ScoreDto

    companion object{
       const val BASE_URL = "https://android-interview.s3.eu-west-2.amazonaws.com/"
    }
}