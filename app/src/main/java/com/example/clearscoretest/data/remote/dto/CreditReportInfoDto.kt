package com.example.clearscoretest.data.remote.dto


import com.example.clearscoretest.domain.model.CreditReportInfo
import com.google.gson.annotations.SerializedName

data class CreditReportInfoDto(
    @SerializedName("score")
    val score: Int,
    @SerializedName("maxScoreValue")
    val maxScoreValue: Int
){
    fun toCreditReportInfo(): CreditReportInfo{
        return CreditReportInfo(
            score = score,
            maxScoreValue = maxScoreValue
        )
    }
}