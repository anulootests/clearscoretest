package com.example.clearscoretest.data.repository

import com.example.clearscoretest.domain.model.CreditReportInfo
import com.example.clearscoretest.domain.model.Score
import com.example.clearscoretest.domain.repository.ScoreRepository
import com.example.clearscoretest.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FakeRepository : ScoreRepository {
    private val creditReportInfo = CreditReportInfo(
        score = 500,
        maxScoreValue = 700
    )
    private  val score = Score(
        creditReportInfo = creditReportInfo
    )
    override fun getScore(): Flow<Resource<Score>> {
        return flow { emit(Resource.Success(score)) }
    }
}