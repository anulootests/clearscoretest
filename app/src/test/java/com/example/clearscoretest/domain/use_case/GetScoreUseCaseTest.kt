package com.example.clearscoretest.domain.use_case

import com.example.clearscoretest.data.repository.FakeRepository
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class GetScoreUseCaseTest {
    private lateinit var fakeRepository: FakeRepository
    private lateinit var getScoreUseCase: GetScoreUseCase

    @Before
    fun setUp() {
        fakeRepository = FakeRepository()
        getScoreUseCase = GetScoreUseCase(fakeRepository)
    }

    @Test
    fun `Is current score 500`() {
        runBlocking {
            getScoreUseCase().collect {
                val score = it.data?.creditReportInfo?.score
                assertThat(score).isEqualTo(500)
            }
        }
    }

    @Test
    fun `Is max score 700`() {
        runBlocking {
            getScoreUseCase().collect {
                val score = it.data?.creditReportInfo?.maxScoreValue
                assertThat(score).isEqualTo(700)
            }
        }
    }
}